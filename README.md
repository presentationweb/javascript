# JavaScript

## Description

Ce GitLab a pour but de présenter le site web réalisé entièrement en JavaScript durant mon DUT Informatique.
Les objectifs de mes TP sont visibles sur le PDF Fils-Rouge.

## Mise en place

Commande pour faire fonctionner le site web :

```
npm install grunt-bootstrap
npm install uglify-js
npm install grunt-contrib-copy
npm install grunt-contrib-uglify
```
