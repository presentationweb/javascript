function onClick() {
	var titre = $('h1');
	var search = $('input');

	changerCouleurListe('h1,h2,p.text-news', couleur_noir);
	if(!controleChamps(search, titre)){
		titre.text("Search");
		return;
	}
	setText(search, titre);
	recherche(search.val(), titre);
}

function recherche(word, titre){
	var listeElement = $('h2,p.text-news');
	changerCouleurListe('h2,p.text-news', couleur_noir);
	var detecteElement = false;
	listeElement.each(function(){
		text = $(this).html();
		if(text.search(word) != -1){
			detecteElement = true;
			changerCouleur($(this), couleur_bleu);
		}
	});
	if(!detecteElement){
		logAvertissement('Aucun élément contenant le mot '+word+' a été détecté !');
		changerCouleur(titre, couleur_rouge);
	}
}

function setText(source, cible){
	cible.text(source.val());
}

function controleChamps(element, titre){
	var zoneErreur = $('span#error-message');
	if(element.val() == ''){
		zoneErreur.text('/!\\ Aucune valeur écrite /!\\');
		logErreur('Aucune valeur est renseignée dans la barre de recherche !');
		changerCouleur(zoneErreur, couleur_rouge);
		return false;
	}
	if(element.val().trim() == ''){
		zoneErreur.text('/!\\ Problème dans votre recherche (voir log message) /!\\');
		logAvertissement('Vous avez mis seulement des caractères blanc ! Merci de mettre au minimum une lettre ou un chiffre !');
		changerCouleur(zoneErreur, couleur_rouge);
		return false;
	}
	else{
		zoneErreur.text('');
		changerCouleur(zoneErreur, couleur_noir);
		return true;
	}
}