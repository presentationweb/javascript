function chargementJSON(elementJSON){
	return JSON.parse(allNewsJSON);
}

function ajouterElement(source, cible){
	source.append(cible);
}

function ajouterElementDiv(){
	return $('<div class="col-md-4"></div>');
}

function ajouterElementH2(element){
	return $('<h2>'+ element.title +'</h2>');
}

function ajouterElementP(element){
	return $('<p class="text-news">'+ element.desc +'</p>');
}

function ajouterElementA(element){ // Mettre des $ et regarder les erreurs (en boucle)
	return $('<p><a class="btn btn-secondary" href="#" role="button">View details »</a></p>');
}

function creationHTMLdansParent(element, id){
	div = ajouterElementDiv();
	h2 = ajouterElementH2(element);
	p = ajouterElementP(element);
	a = ajouterElementA();
	
	ajouterElement(div, h2);
	ajouterElement(div, p);
	ajouterElement(div, a);
	ajouterElement(currentDiv, div);
	setID(div, id);
}

function allNewsJSONtoHTMLdansElement(parseElements){
	var id = 0;
	parseElements.forEach(function(element){
		creationHTMLdansParent(element, id);
		id++;
	});
}