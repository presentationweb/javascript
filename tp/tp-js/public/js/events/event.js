function chargementEvents(){
	initEventButton();
	initEventA();
	initEventMenu();
}

function rechargementEvents(){
	initEventA();
}

function initEventButton(){
	$('button.btn.btn-outline-light').click(function(event){
		event.preventDefault();
		onClick();
	});
	$('button.btn.btn-success').click(function(event){
		controleFormulaire();
	});
}

function initEventA(){
	$('a').click(function(event){
		onDetail($(this));
	});
}

function initEventMenu(){
	$('a.nav-link.news').click(function(event){
		visible(divNews);
		invisible(divCreation);
	})
	$('a.nav-link.creation').click(function(event){
		visible(divCreation);
		invisible(divNews);
	})
}