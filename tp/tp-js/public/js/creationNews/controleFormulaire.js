function controleTitre(titre){
	/*if(titre != undefined && titre.length > 0 && titre.length < 51)
		logMessage('Titre de taille correct ('+ titre.length +'/50)');*/
	if(titre == ''){
		logErreur('Une News doit obligatoirement avoir un titre !');
		erreurFormulaire('Vous devez mettre un titre à votre News');
		return false;
	}
	if(titre.length > 50){
		logErreur('Une News doit obligatoirement avoir un titre inférieur à 50 caractères !');
		logAvertissement('Ici vous avez '+ titre.length +' caractères sur 50');
		erreurFormulaire('Votre titre est supérieur à 50 caractères ('+ titre.length +'/50)');
		return false;
	}
	return true;
}

function controleDescription(description){
	if(description == ''){
		logErreur('Une News doit obligatoirement avoir une description !');
		erreurFormulaire('Vous devez mettre une description à votre News');
		return false;
	}
	return true;
}

function controleFormulaire(){
	effacerErreurFormulaire();
	var titre = $('input#titreNews');
	var desc = $('textarea#descriptionNews');
	if(!controleTitre(titre.val())){
		logMessage('titre faux !');
		return;
	}
	if(!controleDescription(desc.val())){
		logMessage('desc faux !');
		return;
	}
	logMessage('News valide :\n\tTitre : '+ titre.val() +'\n\tDescription : '+ desc.val());
	ajoutNews(titre.val(), desc.val());
}

function effacerErreurFormulaire(){
	$('span#error-message-formulaire').text('');
}

function erreurFormulaire(message){
	$('span#error-message-formulaire').text('/!\\ '+ message +' /!\\');
}