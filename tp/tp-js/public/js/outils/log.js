function logMessage(message) {
	console.log(message);
}

function logErreur(message){
	console.error(message);
}

function logAvertissement(message){
	console.warn(message);
}

function logHtmlElements(selector){
	$(selector).each(function(){
		logMessage($(this).html());
	});
}