function changerCouleur(element, couleur){
	$(element).css('color', couleur);
}

function changerCouleurListe(cible, couleur){
	$(cible).each(function(){
		$(this).css('color', couleur);
	});
}

function visible(div){
	$(div).show();
}

function invisible(div){
	$(div).hide();
}