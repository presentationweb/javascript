module.exports = function(grunt) {

  // Project configuration.
	grunt.initConfig({
		copy: {
			main: {
				files: [
					// includes files within path
					{expand: false, src: ['node_modules/bootstrap/dist/css/bootstrap.min.css'], dest: 'tp-js/public/css/bootstrap.min.css', filter: 'isFile'},
					{expand: false, src: ['node_modules/jquery/dist/jquery.min.js'], dest: 'tp-js/public/js/jquery.min.js', filter: 'isFile'},
					{expand: false, src: ['node_modules/vue/dist/vue.min.js'], dest: 'tp-js/public/js/vue.min.js', filter: 'isFile'},
				],
			},
		},
		uglify: {
			my_target: {
				files: {
					'tp-js/public/js/app.min.js': ['tp-js/public/js/outils/style.js', 'tp-js/public/js/outils/var.js', 'tp-js/public/js/outils/log.js', 'tp-js/public/js/news/detail.js', 'tp-js/public/js/news/gestionElement.js', 'tp-js/public/js/news/news.js', 'tp-js/public/js/news/search.js', 'tp-js/public/js/events/event.js', 'tp-js/public/js/creationNews/controleFormulaire.js', 'tp-js/public/js/creationNews/creationNews.js', 'tp-js/public/js/creationNews/gestionID.js', 'tp-js/public/js/main.js']
				}
			},
		},
  });

  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-uglify');

  grunt.registerTask('default', ['copy', 'uglify']);
  grunt.registerTask('minim', ['uglify']);
};